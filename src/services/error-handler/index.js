exports.errorHandler = errorHandler

function errorHandler() {
  return function (err, req, res, next) {

    if (req.bodymen && req.bodymen.error) {
      if (req.bodymen.error.name === 'minlength') {
        res.status(400).json({
          success: false,
          message: req.bodymen.error.param + '_too_short'
        })
      }
      if (req.bodymen.error.name === 'required') {
        res.status(400).json({
          success: false,
          message: req.bodymen.error.param + '_is_required'
        })
      }
      if (req.bodymen.error.name === 'match' || req.bodymen.error.name === 'enum') {
        res.status(422).json({
          success: false,
          message: req.bodymen.error.param + '_is_invalid'
        })
      }
    } else if (err) {


      if (err.name === 'CastError') {
        res.status(422).json({
          success: false,
          message: err.path.replace(/[\W_]+/g, '') + '_is_invalid'
        })
      }
      if (err.name === 'ValidationError') {
        res.status(422).json({
          success: false,
          message: err.message.split('`')[1] + '_is_invalid'
        })
      }
    } else {
      next(err)
    }
  }
}

exports.default = { errorHandler }
