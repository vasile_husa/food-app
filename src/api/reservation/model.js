import mongoose, { Schema } from 'mongoose'

const status = ['active', 'completed', 'declined']

const reservationSchema = new Schema({
  clientId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  arrivingAt: {
    type: Date,
    required: true
  },
  nrSeats: {
    type: Number,
    required: true
  },
  cash: {
    type: Boolean,
    default: true
  },
  amount: {
    type: Number
  },
  status: {
    type: String,
    enum: status,
    default: 'active'
  },
  tableId: {
    type: Schema.ObjectId,
    ref: 'Table',
    required: false
  },
  restaurantName:{
    type:String
  },
  restaurantId: {
    type: Schema.ObjectId,
    ref: 'Restaurant',
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

reservationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      clientId: this.clientId.view(),
      arrivingAt: this.arrivingAt,
      nrSeats: this.nrSeats,
      cash: this.cash,
      amount: this.amount,
      status: this.status,
      restaurantName: this.restaurantName,
      restaurantId: this.restaurantId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Reservation', reservationSchema)

export const schema = model.schema
export default model
