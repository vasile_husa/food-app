import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy, getOwned, getReservationByRestaurantId } from './controller'
import { schema } from './model'
export Reservation, { schema } from './model'

const router = new Router()
const { clientId, arrivingAt, nrSeats, cash, amount, status, restaurantName, restaurantId, tableId } = schema.tree

/**
 * @api {post} /reservations Create reservation
 * @apiName CreateReservation
 * @apiGroup Reservation
 * @apiParam {String} access_token access token.
 * @apiParam clientId Reservation's clientId.
 * @apiParam arrivingAt Reservation's arrivingAt.
 * @apiParam nrSeats Reservation's nrSeats.
 * @apiParam cash Reservation's cash.
 * @apiParam amount Reservation's amount.
 * @apiParam status Reservation's status.
 * @apiParam restaurantName Reservation's restaurantName. 
 * @apiParam restaurantId Reservation's restaurantId.
 * @apiParam tableId Reservation's tableId.
 * @apiSuccess {Object} reservation Reservation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reservation not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true }),
  body({ clientId, arrivingAt, nrSeats, cash, amount, status, restaurantName, restaurantId, tableId }),
  create)

/**
 * @api {get} /reservations/owned Retrieve user reservations
 * @apiName RetrieveReservations
 * @apiGroup Reservation
 * @apiParam {String} access_token access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} reservations List of reservations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/owned',
  token({ required: true }),
  getOwned)

/**
 * @api {get} /reservations Retrieve reservations
 * @apiName RetrieveReservations
 * @apiGroup Reservation
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} reservations List of reservations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  token({ required: true, roles: ['admin','manager'] }),
  query(),
  index)

/**
 * @api {get} /reservations/:id Retrieve reservation
 * @apiName RetrieveReservation
 * @apiGroup Reservation
 * @apiParam {String} access_token access token.
 * @apiSuccess {Object} reservation Reservation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reservation not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

  router.get('/restaurantId/:restaurantId',
  token({ required: true }),
  getReservationByRestaurantId)


/**
 * @api {put} /reservations/:id Update reservation
 * @apiName UpdateReservation
 * @apiGroup Reservation
 * @apiParam {String} access_token access token.
 * @apiParam arrivingAt Reservation's arrivingAt.
 * @apiParam nrSeats Reservation's nrSeats.
 * @apiParam cash Reservation's cash.
 * @apiParam amount Reservation's amount.
 * @apiParam status Reservation's status.
 * @apiParam tableId Reservation's tableId.
 * @apiSuccess {Object} reservation Reservation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reservation not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ arrivingAt, nrSeats, cash, amount, status, tableId }),
  update)

/**
 * @api {delete} /reservations/:id Delete reservation
 * @apiName DeleteReservation
 * @apiGroup Reservation
 * @apiParam {String} access_token access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Reservation not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
