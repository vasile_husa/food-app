import {notFound, success} from '../../services/response/'
import {Reservation} from '.'
import {Order} from '../order'

export const create = ({ bodymen: { body } }, res, next) =>
  Reservation.create(body)
    .then((reservation) => reservation.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Reservation.find(query, select, cursor)
    .populate('clientId')
    .then((reservations) => {
      const promise = reservations.map(async (reservation) => {
        let rez = reservation.view()
        rez.order = await Order.find({reservationId: reservation.id})
        return rez
      })
      return Promise.all(promise)
    })
    .then(success(res))
    .catch(next)

export const getOwned = ({ user }, res, next) =>
  Reservation.find({clientId: user.id})
    .then((reservations) => reservations.map((reservation) => reservation.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) => {
  Reservation.findById(params.id)
     .populate('clientId')
    .then(notFound(res))
    .then((reservation) => reservation ? reservation.view() : null)
    .then(success(res))
    .catch(next)
}

export const getReservationByRestaurantId = ({ params }, res, next) => 
  Reservation.find({restaurantId: params.restaurantId})
    .then(notFound(res))
    .then((reservations) => reservations.map((reservation) => reservation.view()))
    .then(success(res))
    .catch(next)


export const update = ({ bodymen: { body }, params }, res, next) =>
  Reservation.findById(params.id)
    .then(notFound(res))
    .then((reservation) => reservation ? Object.assign(reservation, body).save() : null)
    .then((reservation) => reservation ? reservation.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Reservation.findById(params.id)
    .then(notFound(res))
    .then((reservation) => reservation ? reservation.remove() : null)
    .then(success(res, 204))
    .catch(next)
