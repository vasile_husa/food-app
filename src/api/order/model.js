import mongoose, { Schema } from 'mongoose'

const orderSchema = new Schema({
  clientId: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  restaurantId: {
    type: Schema.ObjectId,
    ref: 'Restaurant',
    required: true,
  },
  reservationId:{
    type:Schema.ObjectId,
    ref:'Reservation',
    required:true
  },
  amount:{
    type:Number,
    default: 0
  },
  itemIds: [String]
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

orderSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      clientId: this.clientId,
      restaurantId: this.restaurantId,
      reservationId: this.reservationId,
      itemIds: this.itemIds,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      amount: this.amount
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Order', orderSchema)

export const schema = model.schema
export default model
