import { Router } from 'express'
import { token } from '../../services/passport'
import { show } from './controller'

const router = new Router()

/**
 * @api {get} /menu/:restaurantId Retrieve menu
 * @apiName RetrieveMenu
 * @apiGroup Menu
 * @apiParam {String} access_token access token.
 * @apiSuccess {Object} menu Menu's data.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 [
   {
          "foodCategory": "dfsadsa",
          "dishes": []
   },
   {
          "foodCategory": "test1",
          "dishes": [
              {
                  "categoryId": "5d5c58ed58e3c1c1621e94b8",
                  "title": "Netbook",
                  "description": "Deserunt mollit ex nisi ullamco anim. Do do ipsum magna et magna adipisicing elit deserunt ullamco duis. Elit elit culpa officia consectetur aliqua aliqua commodo. Ipsum anim veniam eiusmod nostrud sint Lorem.",
                  "weight": 414,
                  "image": "http://placehold.it/32x32",
                  "restaurantId": "5d5c589ce4d715c15d34b9e4",
                  "createdAt": "2019-08-20T21:21:20.497Z",
                  "updatedAt": "2019-08-20T21:21:20.497Z",
                  "__v": 0,
                  "id": "5d5c6450c6f15fc70eb2ad89"
              }
          ]
      },
   {
          "foodCategory": "342121",
          "dishes": [
              {
                  "categoryId": "5d5c58f758e3c1c1621e94bb",
                  "title": "Eyewax",
                  "description": "Sint sit non consequat ut laborum qui elit amet voluptate consequat. Velit anim consequat nulla incididunt sit labore eu deserunt laborum eu minim adipisicing laboris ullamco. Velit id occaecat pariatur tempor.",
                  "weight": 697,
                  "image": "http://placehold.it/32x32",
                  "restaurantId": "5d5c589ce4d715c15d34b9e4",
                  "createdAt": "2019-08-20T21:20:45.646Z",
                  "updatedAt": "2019-08-20T21:20:45.646Z",
                  "__v": 0,
                  "id": "5d5c642dc6f15fc70eb2ad87"
              },
              {
                  "categoryId": "5d5c58f758e3c1c1621e94bb",
                  "title": "Zaphire",
                  "description": "Labore esse enim ea fugiat cillum ullamco ex voluptate adipisicing mollit aliquip ea. Est non laborum amet sunt dolore nulla nostrud duis proident sit pariatur. Nostrud aute laboris magna occaecat ex exercitation veniam est. Incididunt cillum magna duis aute occaecat aliqua Lorem id pariatur occaecat. Eu ullamco consectetur id ad quis anim elit aliqua ut dolore pariatur dolor culpa nisi.",
                  "weight": 680,
                  "image": "http://placehold.it/32x32",
                  "restaurantId": "5d5c589ce4d715c15d34b9e4",
                  "createdAt": "2019-08-20T21:21:06.747Z",
                  "updatedAt": "2019-08-20T21:21:06.747Z",
                  "__v": 0,
                  "id": "5d5c6442c6f15fc70eb2ad88"
              }
          ]
      },
   {
        "foodCategory": "Sushi",
        "dishes": []
    }
 ]
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Menu not found.
 * @apiError 401 admin access only.
 */
router.get('/:restaurantId',
  show)

export default router
