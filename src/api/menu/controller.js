import { FoodItem } from '../food-item'
import { FoodCategory } from '../food-category'

export const show = ({ params }, res, next) => {
  return FoodCategory.find({
    restaurantId: params.restaurantId
  })
    .then(async (foodCategories) => {
      let menu = []
      const promise = foodCategories.map(async (category) => {
        let dishes = await FoodItem.find({categoryId: category.id})
        menu.push({foodCategory: category.name, dishes})
      })
      return Promise.all(promise)
        .then(() => {
          res.status(200).json(menu)
          return null
        })
    })
}
