import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, getOwned, show, update, destroy, getCategory } from './controller'
import { schema } from './model'
export Restaurant, { schema } from './model'

const router = new Router()
const { category, name, description, country, city, address, postalCode, logo, images, openAt, closingAt } = schema.tree

/**
 * @api {post} /restaurants Create restaurant
 * @apiName CreateRestaurant
 * @apiGroup Restaurant
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam category Restaurant's category
 * @apiParam name Restaurant's name.
 * @apiParam description Restaurant's description.
 * @apiParam country Restaurant's country.
 * @apiParam city Restaurant's city.
 * @apiParam address Restaurant's address.
 * @apiParam postalCode Restaurant's postalCode.
 * @apiParam logo Restaurant's logo.
 * @apiParam images Restaurant's images.
 * @apiParam managerId Restaurant's managerId.
 * @apiParam openAt Restaurant's opening hour.
 * @apiParam closingAt Restaurant's closing hour.
 * @apiSuccess {Object} restaurant Restaurant's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Restaurant not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['admin', 'manager'] }),
  body({ category, name, description, country, city, address, postalCode, logo, images, openAt, closingAt }),
  create)

/**
 * @api {get} /restaurants Retrieve restaurants
 * @apiName RetrieveRestaurants
 * @apiGroup Restaurant
 * @apiUse listParams
 * @apiSuccess {Object[]} restaurants List of restaurants.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /restaurants/owned Retrieve owned restaurant
 * @apiName RetrieveOwnedRestaurant
 * @apiGroup Restaurant
 * @apiPermission admin, manager
 * @apiParam {String} access_token admin and manager access token.
 * @apiSuccess {Object} restaurant Restaurant's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Restaurant not found.
 * @apiError 401 admin access only.
 */
router.get('/owned',
  token({ required: true, roles: ['admin', 'manager'] }),
  getOwned)

/**
 * @api {get} /restaurants/:id Retrieve restaurant
 * @apiName RetrieveRestaurant
 * @apiGroup Restaurant
 * @apiSuccess {Object} restaurant Restaurant's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Restaurant not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  show)

/**
 * @api {put} /restaurants/:id Update restaurant
 * @apiName UpdateRestaurant
 * @apiGroup Restaurant
 * @apiPermission admin, manager
 * @apiParam {String} access_token admin access token.
 * @apiParam category Restaurant's category
 * @apiParam name Restaurant's name.
 * @apiParam description Restaurant's description.
 * @apiParam country Restaurant's country.
 * @apiParam city Restaurant's city.
 * @apiParam address Restaurant's address.
 * @apiParam postalCode Restaurant's postalCode.
 * @apiParam logo Restaurant's logo.
 * @apiParam images Restaurant's images.
 * @apiParam managerId Restaurant's managerId.
 * @apiSuccess {Object} restaurant Restaurant's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Restaurant not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin', 'manager'] }),
  body({ category, name, description, country, city, address, postalCode, logo, images, openAt, closingAt }),
  update)

/**
 * @api {delete} /restaurants/:id Delete restaurant
 * @apiName DeleteRestaurant
 * @apiGroup Restaurant
 * @apiPermission admin, manager
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Restaurant not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin', 'manager'] }),
  destroy)

/**
 * @api {get} /restaurants/category/:category Retrieve restaurant
 * @apiName RetrieveRestaurantByCategory 
 * @apiGroup Restaurant
 * @apiParam {String} access_token admin access token.
 * @apiSuccess {Object} restaurant Restaurant's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Restaurant not found.
 * @apiError 401 admin access only.
 */

router.get('/category/:category',
  getCategory)

export default router
