import mongoose, { Schema } from 'mongoose'
const categories = ['raw vegan', 'italian', 'chinese', 'indian', 'vegetarian', 'romanian', 'traditional']
const restaurantSchema = new Schema({
  category: {
    type: String,
    required: true,
    enum: categories,
    default: 'romanian'
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  postalCode: {
    type: Number,
    required: true
  },
  logo: {
    type: String
  },
  images: [String],
  managerId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  openAt: {
    type: Number,
    required: false
  },
  closingAt: {
    type: Number,
    required: false
  }
}, {
    timestamps: true,
    toJSON: {
      virtuals: true,
      transform: (obj, ret) => { delete ret._id }
    }
  })

restaurantSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      category: this.category,
      name: this.name,
      description: this.description,
      country: this.country,
      city: this.city,
      address: this.address,
      postalCode: this.postalCode,
      logo: this.logo,
      images: this.images,
      managerId: this.managerId,
      openAt: this.openAt,
      closingAt: this.closingAt,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Restaurant', restaurantSchema)

export const schema = model.schema
export default model
