import { Router } from 'express'
import user from './user'
import auth from './auth'
import restaurant from './restaurant'
import reservation from './reservation'
import foodCategory from './food-category'
import foodItem from './food-item'
import menu from './menu'
import table from './table'
import order from './order'
import upload from './upload'

const router = new Router()

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/users', user)
router.use('/auth', auth)
router.use('/restaurants', restaurant)
router.use('/reservations', reservation)
router.use('/food-categories', foodCategory)
router.use('/food-items', foodItem)
router.use('/menu', menu)
router.use('/tables', table)
router.use('/orders', order)
router.use('/upload', upload)

export default router
