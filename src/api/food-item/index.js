import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy,updateStatus } from './controller'
import { schema } from './model'
export FoodItem, { schema } from './model'

const router = new Router()
const { categoryId, title, description, price, weight, image,status, restaurantId} = schema.tree

/**
 * @api {post} /food-items Create food item
 * @apiName CreateFoodItem
 * @apiGroup FoodItem
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiParam categoryId Food item's categoryId.
 * @apiParam title Food item's title.
 * @apiParam description Food item's description.
 * @apiParam weight Food item's weight.
 * @apiParam image Food item's image.
 * @apiParam status Food item's status.
 * @apiParam restaurantId Food item's restaurantId.
 * @apiSuccess {Object} foodItem Food item's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Food item not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['admin','manager'] }),
  body({ categoryId, title, description, price, weight, image,status, restaurantId }),
  create)

/**
 * @api {get} /food-items Retrieve food items
 * @apiName RetrieveFoodItems
 * @apiGroup FoodItem
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} foodItems List of food items.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /food-items/:id Retrieve food item
 * @apiName RetrieveFoodItem
 * @apiGroup FoodItem
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiSuccess {Object} foodItem Food item's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Food item not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  show)

/**
 * @api {put} /food-items/:id Update food item
 * @apiName UpdateFoodItem
 * @apiGroup FoodItem
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiParam categoryId Food item's categoryId.
 * @apiParam title Food item's title.
 * @apiParam description Food item's description.
 * @apiParam weight Food item's weight.
 * @apiParam image Food item's image.
 * @apiParam status Food item's status.
 * @apiParam restaurantId Food item's restaurantId.
 * @apiSuccess {Object} foodItem Food item's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Food item not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin','manager'] }),
  body({categoryId, title, description, price, weight, image,restaurantId}),
  update)
  
/**
 * @api {put} /food-items/:id/update-status Update food item status
 * @apiName UpdateFoodItem
 * @apiGroup FoodItem
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiParam status Food item's status.
 * @apiSuccess {Object} foodItem Food item's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Food item not found.
 * @apiError 401 admin access only.
 */
 router.put('/:id/update-status',
   token({required: true, roles:['admin','manager'] }),
   body({status}),
   updateStatus)

/**
 * @api {delete} /food-items/:id Delete food item
 * @apiName DeleteFoodItem
 * @apiGroup FoodItem
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Food item not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin', 'manager'] }),
  destroy)

export default router
