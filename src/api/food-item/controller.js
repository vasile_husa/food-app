import { success, notFound } from '../../services/response/'
import { FoodItem } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  FoodItem.create(body)
    .then((foodItem) => foodItem.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  FoodItem.find(query, select, cursor)
    .then((foodItems) => foodItems.map((foodItem) => foodItem.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  FoodItem.findById(params.id)
    .then(notFound(res))
    .then((foodItem) => foodItem ? foodItem.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  FoodItem.findById(params.id)
    .then(notFound(res))
    .then((foodItem) => foodItem ? Object.assign(foodItem, body).save() : null)
    .then((foodItem) => foodItem ? foodItem.view(true) : null)
    .then(success(res))
    .catch(next)

export const updateStatus = ({ bodymen: { body }, params }, res, next) =>
    FoodItem.findById(params.id)
      .then(notFound(res))
      .then((foodItem) => foodItem ? Object.assign(foodItem, body).save() : null)
      .then((foodItem) => foodItem ? foodItem.view(true) : null)
      .then(success(res))
      .catch(next)

export const destroy = ({ params }, res, next) =>
  FoodItem.findById(params.id)
    .then(notFound(res))
    .then((foodItem) => foodItem ? foodItem.remove() : null)
    .then(success(res, 204))
    .catch(next)
