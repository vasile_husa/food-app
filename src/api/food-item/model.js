import mongoose, { Schema } from 'mongoose'

const status = ['active','innactive']

const foodItemSchema = new Schema({
  categoryId: {
    type: Schema.ObjectId,
    ref: 'FoodCategory',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    default: 0,
    required: true
  },
  weight: {
    type: Number,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  status:{
    type: String,
    enum: status,
    default: 'active'
    
  },
  restaurantId: {
    type: Schema.ObjectId,
    ref: 'Restaurant',
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

foodItemSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      categoryId: this.categoryId,
      title: this.title,
      description: this.description,
      weight: this.weight,
      image: this.image,
      status: this.status,
      restaurantId: this.restaurantId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('FoodItem', foodItemSchema)

export const schema = model.schema
export default model
