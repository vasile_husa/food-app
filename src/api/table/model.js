import mongoose, { Schema } from 'mongoose'

const location = ['inside', 'outside']

const tableSchema = new Schema({
  name: {
    type: String
  },
  nrSeats: {
    type: Number
  },
  location: {
    type: String,
    enum: location,
    default: 'inside'
  },
  restaurantId: {
    type: Schema.ObjectId,
    ref: 'Restaurant',
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

tableSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      nrSeats: this.nrSeats,
      location: this.location,
      restaurantId: this.restaurantId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Table', tableSchema)

export const schema = model.schema
export default model
