import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Table, { schema } from './model'

const router = new Router()
const { name, nrSeats, location, restaurantId } = schema.tree

/**
 * @api {post} /tables Create table
 * @apiName CreateTable
 * @apiGroup Table
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiParam name Table's name.
 * @apiParam nrSeats Table's nrSeats.
 * @apiParam location Table's location.
 * @apiParam restaurantId Table's restaurantId.
 * @apiSuccess {Object} table Table's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Table not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['admin','manager'] }),
  body({ name, nrSeats, location, restaurantId }),
  create)

/**
 * @api {get} /tables Retrieve tables
 * @apiName RetrieveTables
 * @apiGroup Table
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} tables List of tables.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  token({ required: true, roles: ['admin','manager'] }),
  query(),
  index)

/**
 * @api {get} /tables/:id Retrieve table
 * @apiName RetrieveTable
 * @apiGroup Table
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiSuccess {Object} table Table's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Table not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  token({ required: true, roles: ['admin','manager'] }),
  show)

/**
 * @api {put} /tables/:id Update table
 * @apiName UpdateTable
 * @apiGroup Table
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiParam name Table's name.
 * @apiParam nrSeats Table's nrSeats.
 * @apiParam location Table's location.
 * @apiParam restaurantId Table's restaurantId.
 * @apiSuccess {Object} table Table's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Table not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin','manager'] }),
  body({ name, nrSeats, location, restaurantId }),
  update)

/**
 * @api {delete} /tables/:id Delete table
 * @apiName DeleteTable
 * @apiGroup Table
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Table not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin','manager'] }),
  destroy)

export default router
