import { Router } from 'express'

const router = new Router()

/**
 * @api {post} https://api.cloudinary.com/v1_1/dinout/image/upload Upload image
 * @apiName UploadImage
 * @apiGroup Upload
 * @apiParam {String} access_token access token.
 * @apiParam {File} file the image file.
 * @apiParam {String} upload_preset This MUST be `gesbqell`.
 * @apiSuccess {Object} upload Upload's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Upload not found.
 * @apiError 401 admin access only.
 */

export default router
