import { success, notFound } from '../../services/response/'
import { FoodCategory } from '.'

export const create = ({ bodymen: { body }, user }, res, next) =>
  FoodCategory.create({...body, restaurantId: user.restaurantId})
    .then((foodCategory) => foodCategory.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor }, user }, res, next) =>
  FoodCategory.find({...query, restaurantId: user.restaurantId}, select, cursor)
    .then((foodCategories) => foodCategories.map((foodCategory) => foodCategory.view()))
    .then(success(res))
    .catch(next)

export const getByRestaurant = ({ params }, res, next) =>
  FoodCategory.find({restaurantId: params.restaurantId})
    .then((foodCategories) => foodCategories.map((foodCategory) => foodCategory.view()))
    .then(success(res))
    .catch(next)


export const destroy = ({ params }, res, next) =>
  FoodCategory.findById(params.id)
    .then(notFound(res))
    .then((foodCategory) => foodCategory ? foodCategory.remove() : null)
    .then(success(res, 204))
    .catch(next)
