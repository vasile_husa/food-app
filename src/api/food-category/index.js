import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, destroy, getByRestaurant } from './controller'
import { schema } from './model'
export FoodCategory, { schema } from './model'

const router = new Router()
const { name } = schema.tree

/**
 * @api {post} /food-categories Create food category
 * @apiName CreateFoodCategory
 * @apiGroup FoodCategory
 * @apiPermission admin, manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiParam name Food category's name.
 * @apiSuccess {Object} foodCategory Food category's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Food category not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['admin', 'manager'] }),
  body({ name }),
  create)

/**
 * @api {get} /food-categories Retrieve food categories
 * @apiName RetrieveFoodCategories
 * @apiGroup FoodCategory
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} foodCategories List of food categories.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  token({ required: true, roles: ['admin', 'manager'] }),
  query(),
  index)

/**
 * @api {get} /food-categories/restaurant/:restaurantId Retrieve food categories
 * @apiName RetrieveFoodCategories
 * @apiGroup FoodCategory
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} foodCategories List of food categories.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/restaurant/:restaurantId',
  token({ required: true }),
  getByRestaurant)

/**
 * @api {delete} /food-categories/:id Delete food category
 * @apiName DeleteFoodCategory
 * @apiGroup FoodCategory
 * @apiPermission admin,manager
 * @apiParam {String} access_token admin or manager access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Food category not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin', 'manager'] }),
  destroy)

export default router
