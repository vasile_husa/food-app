import mongoose, { Schema } from 'mongoose'

const foodCategorySchema = new Schema({
  name: {
    type: String
  },
  restaurantId: {
    type: Schema.ObjectId,
    ref: 'Restaurant'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

foodCategorySchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      restaurantId: this.restaurantId,
      name: this.name,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('FoodCategory', foodCategorySchema)

export const schema = model.schema
export default model
