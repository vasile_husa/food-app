import { success, notFound } from '../../services/response/'
import { User } from '.'
const nodemailer = require('nodemailer')

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  User.find(query, select, cursor)
    .then((users) => users.map((user) => user.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const showMe = ({ user }, res) =>
  res.json(user.view(true))

export const create = ({ bodymen: { body } }, res, next) =>
  User.create(body)
    .then((user) => user.view(true))
    .then(success(res, 201))
    .catch((err) => {
      /* istanbul ignore else */
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          success: false,
          message: 'email_already_registered'
        })
      } else {
        next(err)
      }
    })

export const update = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isAdmin = user.role === 'admin'
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate && !isAdmin) {
        res.status(401).json({
          success: false,
          message: 'unauthorized'
        })
        return null
      }
      return result
    })
    .then((user) => user ? Object.assign(user, body).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const updatePassword = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate) {
        res.status(401).json({
          success: false,
          message: 'unauthorized'
        })
        return null
      }
      return result
    })
    .then((user) => user ? user.set({ password: body.password }).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const forgotPassword = (req, res, next) => {
  if (req.body.email === '')
    res.json('email required')

  User.findOne({
    email: req.body.email
  }).then(user => {
    if (user === null)
      res.json('email is not in db')
    else {

      const transporter = nodemailer.createTransport({
        service: 'SendGrid',
        auth: {
          user: 'vasilehusa',
          pass: 'vasile1809'
        }
      })

      User.findOne({ email: req.body.email })
        .then(notFound(res))
        .then((user) => user ? user.set({ password: '123456789' }).save() : null)
        .then(success(res))
        .catch(next)

      const mailOptions = {
        from: 'vasile.husa@gmail.com',
        to: user.email,
        subject: 'Link to reset password',
        text: 'Your new password is: 123456789'
      }

      transporter.sendMail(mailOptions, function (err, res) {
        if (err)
          return null
      })
      res.status(200).json('recovery email sent final')
    }
  })
}

export const destroy = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.remove() : null)
    .then(success(res, 204))
    .catch(next)
