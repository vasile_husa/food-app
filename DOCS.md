# food-api v0.0.0



- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [FoodCategory](#foodcategory)
	- [Create food category](#create-food-category)
	- [Delete food category](#delete-food-category)
	- [Retrieve food categories](#retrieve-food-categories)
	
- [FoodItem](#fooditem)
	- [Create food item](#create-food-item)
	- [Delete food item](#delete-food-item)
	- [Retrieve food item](#retrieve-food-item)
	- [Retrieve food items](#retrieve-food-items)
	- [Update food item](#update-food-item)
	
- [Menu](#menu)
	- [Retrieve menu](#retrieve-menu)
	
- [Order](#order)
	- [Create order](#create-order)
	- [Delete order](#delete-order)
	- [Retrieve order](#retrieve-order)
	- [Retrieve orders for restaurant](#retrieve-orders-for-restaurant)
	- [Update order](#update-order)
	
- [Reservation](#reservation)
	- [Create reservation](#create-reservation)
	- [Delete reservation](#delete-reservation)
	- [Retrieve reservation](#retrieve-reservation)
	- [Retrieve user reservations](#retrieve-user-reservations)
	- [Update reservation](#update-reservation)
	
- [Restaurant](#restaurant)
	- [Create restaurant](#create-restaurant)
	- [Delete restaurant](#delete-restaurant)
	- [Retrieve owned restaurant](#retrieve-owned-restaurant)
	- [Retrieve restaurant](#retrieve-restaurant)
	- [Retrieve restaurants](#retrieve-restaurants)
	- [Update restaurant](#update-restaurant)
	
- [Table](#table)
	- [Create table](#create-table)
	- [Delete table](#delete-table)
	- [Retrieve table](#retrieve-table)
	- [Retrieve tables](#retrieve-tables)
	- [Update table](#update-table)
	
- [Upload](#upload)
	- [Upload image](#upload-image)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# FoodCategory

## Create food category



	POST /food-categories


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| name			| 			|  <p>Food category's name.</p>							|

## Delete food category



	DELETE /food-categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|

## Retrieve food categories



	GET /food-categories/restaurant/:restaurantId


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

# FoodItem

## Create food item



	POST /food-items


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| categoryId			| 			|  <p>Food item's categoryId.</p>							|
| title			| 			|  <p>Food item's title.</p>							|
| description			| 			|  <p>Food item's description.</p>							|
| weight			| 			|  <p>Food item's weight.</p>							|
| image			| 			|  <p>Food item's image.</p>							|
| restaurantId			| 			|  <p>Food item's restaurantId.</p>							|

## Delete food item



	DELETE /food-items/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|

## Retrieve food item



	GET /food-items/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|

## Retrieve food items



	GET /food-items


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update food item



	PUT /food-items/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| categoryId			| 			|  <p>Food item's categoryId.</p>							|
| title			| 			|  <p>Food item's title.</p>							|
| description			| 			|  <p>Food item's description.</p>							|
| weight			| 			|  <p>Food item's weight.</p>							|
| image			| 			|  <p>Food item's image.</p>							|
| restaurantId			| 			|  <p>Food item's restaurantId.</p>							|

# Menu

## Retrieve menu



	GET /menu/:restaurantId


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|

### Success Response

Success-Response:

```
   HTTP/1.1 200 OK
[
  {
         "foodCategory": "dfsadsa",
         "dishes": []
  },
  {
         "foodCategory": "test1",
         "dishes": [
             {
                 "categoryId": "5d5c58ed58e3c1c1621e94b8",
                 "title": "Netbook",
                 "description": "Deserunt mollit ex nisi ullamco anim. Do do ipsum magna et magna adipisicing elit deserunt ullamco duis. Elit elit culpa officia consectetur aliqua aliqua commodo. Ipsum anim veniam eiusmod nostrud sint Lorem.",
                 "weight": 414,
                 "image": "http://placehold.it/32x32",
                 "restaurantId": "5d5c589ce4d715c15d34b9e4",
                 "createdAt": "2019-08-20T21:21:20.497Z",
                 "updatedAt": "2019-08-20T21:21:20.497Z",
                 "__v": 0,
                 "id": "5d5c6450c6f15fc70eb2ad89"
             }
         ]
     },
  {
         "foodCategory": "342121",
         "dishes": [
             {
                 "categoryId": "5d5c58f758e3c1c1621e94bb",
                 "title": "Eyewax",
                 "description": "Sint sit non consequat ut laborum qui elit amet voluptate consequat. Velit anim consequat nulla incididunt sit labore eu deserunt laborum eu minim adipisicing laboris ullamco. Velit id occaecat pariatur tempor.",
                 "weight": 697,
                 "image": "http://placehold.it/32x32",
                 "restaurantId": "5d5c589ce4d715c15d34b9e4",
                 "createdAt": "2019-08-20T21:20:45.646Z",
                 "updatedAt": "2019-08-20T21:20:45.646Z",
                 "__v": 0,
                 "id": "5d5c642dc6f15fc70eb2ad87"
             },
             {
                 "categoryId": "5d5c58f758e3c1c1621e94bb",
                 "title": "Zaphire",
                 "description": "Labore esse enim ea fugiat cillum ullamco ex voluptate adipisicing mollit aliquip ea. Est non laborum amet sunt dolore nulla nostrud duis proident sit pariatur. Nostrud aute laboris magna occaecat ex exercitation veniam est. Incididunt cillum magna duis aute occaecat aliqua Lorem id pariatur occaecat. Eu ullamco consectetur id ad quis anim elit aliqua ut dolore pariatur dolor culpa nisi.",
                 "weight": 680,
                 "image": "http://placehold.it/32x32",
                 "restaurantId": "5d5c589ce4d715c15d34b9e4",
                 "createdAt": "2019-08-20T21:21:06.747Z",
                 "updatedAt": "2019-08-20T21:21:06.747Z",
                 "__v": 0,
                 "id": "5d5c6442c6f15fc70eb2ad88"
             }
         ]
     },
  {
       "foodCategory": "Sushi",
       "dishes": []
   }
]
```
# Order

## Create order



	POST /orders


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|
| restaurantId			| 			|  <p>Order's restaurantId.</p>							|
| items			| 			|  <p>Order's items.</p>							|

## Delete order



	DELETE /orders/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|

## Retrieve order



	GET /orders/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_tokeN			| String			|  <p>access token.</p>							|

## Retrieve orders for restaurant



	GET /orders/restaurant/:restaurantId


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update order



	PUT /orders/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|
| restaurantId			| 			|  <p>Order's restaurantId.</p>							|
| items			| 			|  <p>Order's items.</p>							|

# Reservation

## Create reservation



	POST /reservations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|
| clientId			| 			|  <p>Reservation's clientId.</p>							|
| arrivingAt			| 			|  <p>Reservation's arrivingAt.</p>							|
| nrSeats			| 			|  <p>Reservation's nrSeats.</p>							|
| cash			| 			|  <p>Reservation's cash.</p>							|
| amount			| 			|  <p>Reservation's amount.</p>							|
| orderId			| 			|  <p>Reservation's orderId.</p>							|
| status			| 			|  <p>Reservation's status.</p>							|
| restaurantId			| 			|  <p>Reservation's restaurantId.</p>							|
| tableId			| 			|  <p>Reservation's tableId.</p>							|

## Delete reservation



	DELETE /reservations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|

## Retrieve reservation



	GET /reservations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|

## Retrieve user reservations



	GET /reservations/owned


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update reservation



	PUT /reservations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|
| arrivingAt			| 			|  <p>Reservation's arrivingAt.</p>							|
| nrSeats			| 			|  <p>Reservation's nrSeats.</p>							|
| cash			| 			|  <p>Reservation's cash.</p>							|
| amount			| 			|  <p>Reservation's amount.</p>							|
| status			| 			|  <p>Reservation's status.</p>							|
| tableId			| 			|  <p>Reservation's tableId.</p>							|

# Restaurant

## Create restaurant



	POST /restaurants


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Restaurant's name.</p>							|
| description			| 			|  <p>Restaurant's description.</p>							|
| country			| 			|  <p>Restaurant's country.</p>							|
| city			| 			|  <p>Restaurant's city.</p>							|
| address			| 			|  <p>Restaurant's address.</p>							|
| postalCode			| 			|  <p>Restaurant's postalCode.</p>							|
| logo			| 			|  <p>Restaurant's logo.</p>							|
| images			| 			|  <p>Restaurant's images.</p>							|
| managerId			| 			|  <p>Restaurant's managerId.</p>							|
| openAt			| 			|  <p>Restaurant's opening hour.</p>							|
| closingAt			| 			|  <p>Restaurant's closing hour.</p>							|

## Delete restaurant



	DELETE /restaurants/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve owned restaurant



	GET /restaurants/owned


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin and manager access token.</p>							|

## Retrieve restaurant



	GET /restaurants/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve restaurants



	GET /restaurants


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update restaurant



	PUT /restaurants/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Restaurant's name.</p>							|
| description			| 			|  <p>Restaurant's description.</p>							|
| country			| 			|  <p>Restaurant's country.</p>							|
| city			| 			|  <p>Restaurant's city.</p>							|
| address			| 			|  <p>Restaurant's address.</p>							|
| postalCode			| 			|  <p>Restaurant's postalCode.</p>							|
| logo			| 			|  <p>Restaurant's logo.</p>							|
| images			| 			|  <p>Restaurant's images.</p>							|
| managerId			| 			|  <p>Restaurant's managerId.</p>							|

# Table

## Create table



	POST /tables


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| name			| 			|  <p>Table's name.</p>							|
| nrSeats			| 			|  <p>Table's nrSeats.</p>							|
| location			| 			|  <p>Table's location.</p>							|
| restaurantId			| 			|  <p>Table's restaurantId.</p>							|

## Delete table



	DELETE /tables/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|

## Retrieve table



	GET /tables/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|

## Retrieve tables



	GET /tables


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update table



	PUT /tables/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or manager access token.</p>							|
| name			| 			|  <p>Table's name.</p>							|
| nrSeats			| 			|  <p>Table's nrSeats.</p>							|
| location			| 			|  <p>Table's location.</p>							|
| restaurantId			| 			|  <p>Table's restaurantId.</p>							|

# Upload

## Upload image



	POST https://api.cloudinary.com/v1_1/dinout/image/upload


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|
| file			| File			|  <p>the image file.</p>							|
| upload_preset			| String			|  <p>This MUST be <code>gesbqell</code>.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| firstName			| String			| **optional** <p>User's first name.</p>							|
| lastName			| String			| **optional** <p>User's last name.</p>							|
| role			| String			| **optional** <p>User's role.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| firstName			| String			| **optional** <p>User's first name.</p>							|
| lastName			| String			| **optional** <p>User's last name.</p>							|


